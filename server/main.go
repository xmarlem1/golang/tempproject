package main

import (
  "fmt"
  "log"
  "net/http"
  "os"
)

func main() {

  http.HandleFunc("/changes", func(w http.ResponseWriter, r *http.Request) {
    if r.Method == "GET" {
      data, err := os.ReadFile("changes.json")
      if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
      }

      w.Header().Set("Content-Type", "application/json")
      //time.Sleep(20 * time.Second)
      w.WriteHeader(http.StatusOK)
      fmt.Fprintf(w, string(data))
    } else {
      http.Error(w, "Invalid HTTP method specified", http.StatusMethodNotAllowed)
    }
  })

  log.Fatal(http.ListenAndServe(":8888", nil))

}
