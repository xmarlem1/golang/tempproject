package main

import (
  "context"
  "fmt"
  "os"
  "regexp"
  "sample-http-client/client"
  "time"

  "google.golang.org/protobuf/types/known/timestamppb"
)

var (
  url_old  string = "https://jsonplaceholder.typicode.com/posts"
  url      string = "http://localhost:8888/changes"
  changeID        = regexp.MustCompile(`\S+/\S+/(?P<changeID>\S+)`).FindStringSubmatch
)

func main() {
  cli := client.NewClientWithTimeout(5 * time.Second)

  resp, err := cli.GetChanges(context.Background(), url)

  if err != nil {
    fmt.Fprintf(os.Stderr, "%v\n", err)
    os.Exit(1)
  }

  f, err := os.OpenFile("changes.txt", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0700)
  if err != nil {
    fmt.Fprintf(os.Stderr, "%v\n", err)
    os.Exit(1)
  }
  defer f.Close()

  if resp != nil {
    for _, c := range resp.Notes {
      if c.Name != "" {
        cName := changeID(c.Name)
        f.WriteString(fmt.Sprintf("%s\n", cName[1]))
      }
    }
  }

  // getData()
  // playWithtime(7)
}

func playWithtime(days int) {

  currentTime := timestamppb.Now()
  ts := currentTime.AsTime().AddDate(0, 0, -days).Format(time.RFC3339)
  fmt.Println(ts)
}
