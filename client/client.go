package client

import (
  "context"
  "encoding/json"
  "fmt"
  "io"
  "io/ioutil"
  "net/http"
  "net/http/httptrace"
  "time"
)

type client struct {
  httpClient *http.Client
}

func NewClient() *client {
  return &client{
    httpClient: http.DefaultClient,
  }
}

func NewClientWithTimeout(timeout time.Duration) *client {

  transport := &http.Transport{IdleConnTimeout: 10 * time.Second}
  return &client{
    httpClient: &http.Client{
      Timeout:   timeout,
      Transport: transport,
    },
  }
}

func createHTTPGetChangesRequestWithTrace(ctx context.Context, url string) (*http.Request, error) {
  req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
  if err != nil {
    return nil, err
  }
  trace := &httptrace.ClientTrace{
    DNSDone: func(dnsInfo httptrace.DNSDoneInfo) {
      fmt.Printf("DNS Info: %+v\n", dnsInfo)
    },
    GotConn: func(connInfo httptrace.GotConnInfo) {
      fmt.Printf("Got Conn: %+v\n", connInfo)
    },
  }
  ctxTrace := httptrace.WithClientTrace(req.Context(), trace)
  req = req.WithContext(ctxTrace)
  return req, err
}

func (c *client) GetChanges(ctx context.Context, url string) (*ResponseChangeTicket, error) {

  var changes *ResponseChangeTicket
  req, err := createHTTPGetChangesRequestWithTrace(ctx, url)
  if err != nil {
    return nil, err
  }

  // for { //i := 0; i < 3; i++ {
  //   r, err := c.httpClient.Do(req)
  //   if err != nil {
  //     return nil, err
  //   }
  //   time.Sleep(1 * time.Second)
  //   io.Copy(ioutil.Discard, r.Body)
  //   r.Body.Close()
  //   fmt.Println("--------")
  // }

  resp, err := c.httpClient.Do(req)
  if err != nil {
    return nil, err
  }

  defer func() {
    io.Copy(ioutil.Discard, resp.Body)
    resp.Body.Close()

  }()

  if resp.Header.Get("content-type") != "application/json" {
    return changes, nil
  }

  // reading from response body
  data, err := io.ReadAll(resp.Body)
  if err != nil {
    return changes, err
  }

  // checking status code, if not 200, return error
  if resp.StatusCode != http.StatusOK {
    return changes, nil
  }

  err = json.Unmarshal(data, &changes)
  return changes, err
}
