package client

import "time"

type ResponseChangeTicket struct {
  Notes []struct {
    Name                 string    `json:"name"`
    Kind                 string    `json:"kind"`
    CreateTime           time.Time `json:"createTime"`
    UpdateTime           time.Time `json:"updateTime"`
    RelatedNoteNames     []string  `json:"relatedNoteNames"`
    AttestationAuthority struct {
      Hint struct {
        HumanReadableName string `json:"humanReadableName"`
      } `json:"hint"`
    } `json:"attestationAuthority"`
  } `json:"notes"`
}
